#!/bin/sh

DDR_DIR=$(cd "$(dirname "$0")";pwd)
RESULT_DIR=~/Desktop/rockchip_test_logs/ddr
RESULT_LOG=${RESULT_DIR}/ddr_stressapptest.log

#clear log first
rm -rf $RESULT_LOG

if [ ! -e "$RESULT_DIR" ]; then
	echo "no $RESULT_DIR"
	mkdir -p $RESULT_DIR
fi

#run stressapptest_test
echo "**********************DDR STRESSAPPTEST TEST 48H*************************"
echo "***run: stressapptest -s 172800 -i 4 -C 4 -W --stop_on_errors -M 128*****"
echo "******DDR STRESSAPPTEST START: you can see the log at $RESULT_LOG********"
stressapptest -s 60 -i 4 -C 4 -W --stop_on_errors -M 128 -l $RESULT_LOG

