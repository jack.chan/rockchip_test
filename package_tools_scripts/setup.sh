#!/bin/sh
### file: rockchip_test.sh
### author: yhx@rock-chips.com wxt@rock-chips.com
### function: ddr cpu gpio flash bt audio recovery s2r sdio/pcie(wifi)
###           ethernet reboot ddrfreq npu camera video
### date: 20190107
echo $EXTRACT_DIR
cd "$EXTRACT_DIR/rockchip_stress_test"
bash rockchip_test.sh
