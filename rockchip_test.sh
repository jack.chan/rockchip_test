#!/bin/sh
### file: rockchip_test.sh
### author: yhx@rock-chips.com wxt@rock-chips.com
### function: ddr cpu gpio flash bt audio recovery s2r sdio/pcie(wifi)
###           ethernet reboot ddrfreq npu camera video
### date: 20190107

moudle_env()
{
   export  MODULE_CHOICE
   export PATH=$PATH:$(cd "$(dirname "$0")";pwd)/usr/bin
}

ddr_test()
{
    bash ddr/stressapptest_test.sh &
}

cpufreq_test()
{
    echo 'Algo!@Robo#$01%10^18' | sudo -S bash cpu/cpu_freq_stress_test.sh 60 10
}

flash_stress_test()
{
    echo 'Algo!@Robo#$01%10^18' | sudo -S bash flash_test/flash_stress_test.sh 5 20
}

ddr_freq_scaling_test()
{
    bash ddr/ddr_freq_scaling.sh
}

module_test()
{ 
    ddr_test
    cpufreq_test
    flash_stress_test
}
moudle_env
module_test
